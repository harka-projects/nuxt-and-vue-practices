const client_manifest = {
  "_fetch.0bb832a4.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "fetch.0bb832a4.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_vue.f36acd1f.a5a2f194.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "vue.f36acd1f.a5a2f194.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "layouts/default.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "default.efead21f.css",
    "src": "layouts/default.css"
  },
  "layouts/default.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "default.fc0f36d2.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "layouts/default.vue"
  },
  "default.efead21f.css": {
    "file": "default.efead21f.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "layouts/products.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "products.422a7904.css",
    "src": "layouts/products.css"
  },
  "layouts/products.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "products.83bd827a.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "layouts/products.vue"
  },
  "products.422a7904.css": {
    "file": "products.422a7904.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "node_modules/nuxt/dist/app/entry.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "dynamicImports": [
      "layouts/default.vue",
      "layouts/products.vue"
    ],
    "file": "entry.b4f438e6.js",
    "isEntry": true,
    "src": "node_modules/nuxt/dist/app/entry.js",
    "_globalCSS": true
  },
  "pages/about.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "about.625050e4.js",
    "imports": [
      "_fetch.0bb832a4.js",
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/about.vue"
  },
  "pages/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.30ffa907.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/index.vue"
  },
  "pages/products/[id].css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "_id_.1ff6271c.css",
    "src": "pages/products/[id].css"
  },
  "pages/products/[id].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "_id_.52d5460f.js",
    "imports": [
      "_vue.f36acd1f.a5a2f194.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_fetch.0bb832a4.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/products/[id].vue"
  },
  "_id_.1ff6271c.css": {
    "file": "_id_.1ff6271c.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "pages/products/index.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "index.934a37b5.css",
    "src": "pages/products/index.css"
  },
  "pages/products/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "index.e6393c01.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_fetch.0bb832a4.js",
      "_vue.f36acd1f.a5a2f194.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/products/index.vue"
  },
  "index.934a37b5.css": {
    "file": "index.934a37b5.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  }
};

export { client_manifest as default };
//# sourceMappingURL=client.manifest.mjs.map
