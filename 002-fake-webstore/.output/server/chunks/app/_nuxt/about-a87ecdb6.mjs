import { u as useFetch } from './fetch-91b3a6b6.mjs';
import { withAsyncContext, unref, useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrInterpolate } from 'vue/server-renderer';
import '../../nitro/node-server.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'node:fs';
import 'node:url';
import '../server.mjs';
import 'unhead';
import '@unhead/shared';
import 'vue-router';

const _sfc_main = {
  __name: "about",
  __ssrInlineRender: true,
  async setup(__props) {
    let __temp, __restore;
    const { data } = ([__temp, __restore] = withAsyncContext(() => useFetch("/api/currency/GBP", "$c6wy8KmN8o")), __temp = await __temp, __restore(), __temp);
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(_attrs)}><h1>About</h1><p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro, sed numquam in nesciunt veritatis incidunt culpa optio eos debitis nemo! </p><p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Natus hic culpa a. Dolorem voluptatem officia optio quasi repellat voluptate, totam rerum hic ut vel! Porro dignissimos deserunt modi rerum mollitia. </p><p>GBP value: ${ssrInterpolate(unref(data).GBP.value)}</p></div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/about.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=about-a87ecdb6.mjs.map
