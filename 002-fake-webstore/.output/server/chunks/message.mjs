import { a as defineCachedEventHandler, u as useRuntimeConfig } from './nitro/node-server.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'node:fs';
import 'node:url';

const message = defineCachedEventHandler(async () => {
  const { currencyKey } = useRuntimeConfig();
  const { data } = await $fetch(
    `https://api.currencyapi.com/v3/latest?apikey=${currencyKey}`
  );
  return data;
});

export { message as default };
//# sourceMappingURL=message.mjs.map
