export default defineCachedEventHandler(async () => {
  // Handle query parameters
  // const { name } = getQuery(event);

  // Handle post data
  // const { age } = await readBody(event);

  // Get currencyKey
  const { currencyKey } = useRuntimeConfig();

  // API call with private key
  const { data } = await $fetch(
    `https://api.currencyapi.com/v3/latest?apikey=${currencyKey}`
  );

  // return {
  //   message: `Hello, ${name}! You are ${age} years old.`,
  // };

  return data;
});
