export const useUser = defineStore('user', {
  state: () => ({
    isLoggedIn: false,
  }),
  getters: {}, // Define your getters here
  actions: {
    login() {
      this.isLoggedIn = true
      useRouter().push('/')
    },
  }, // Define your actions here
})

if (import.meta.hot) {
  import.meta.hot.accept(() => acceptHMRUpdate(useUser, import.meta.hot))
}
