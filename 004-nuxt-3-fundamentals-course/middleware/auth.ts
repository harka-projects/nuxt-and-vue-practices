export default defineNuxtRouteMiddleware((to, from) => {
  const userIsLoggedIn = false
  if (!userIsLoggedIn) {
    // return abortNavigation('You are not logged in!')
    // return navigateTo('/login')
    // return navigateTo({ name: 'login' })
    return navigateTo({ path: '/login' })
  }
})
