import { d as defineEventHandler, c as createError } from './nitro/node-server.mjs';
import { c as cars } from './cars.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'node:fs';
import 'node:url';
import 'ipx';

const _id__get = defineEventHandler((e) => {
  const { id } = e.context.params;
  const car = cars.find((car2) => {
    return car2.id === parseInt(id);
  });
  if (!car) {
    throw createError({
      statusCode: 404,
      statusMessage: `Car with ID ${id} not found.`
    });
  }
  return car;
});

export { _id__get as default };
//# sourceMappingURL=_id_.get.mjs.map
