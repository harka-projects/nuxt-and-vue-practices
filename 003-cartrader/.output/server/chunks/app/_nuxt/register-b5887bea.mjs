import { _ as _export_sfc, k as useSupabaseClient } from '../server.mjs';
import { ref, mergeProps, useSSRContext } from 'vue';
import { ssrRenderAttrs, ssrRenderAttr, ssrRenderClass, ssrIncludeBooleanAttr, ssrInterpolate } from 'vue/server-renderer';
import '../../nitro/node-server.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'node:fs';
import 'node:url';
import 'ipx';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import '@supabase/supabase-js';

const _sfc_main = {
  __name: "register",
  __ssrInlineRender: true,
  setup(__props) {
    useSupabaseClient();
    const email = ref("");
    const password = ref("");
    const errorMessage = ref(null);
    const successMessage = ref(null);
    const loading = ref(false);
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "font-serif w-[1000px] m-auto text-2xl bg-white flex flex-col items-center justify-center align-center mt-20 text-center" }, _attrs))} data-v-157056b0><form class="w-full max-w-lg text-center" data-v-157056b0><input class="my-4 py-3 px-5 w-full text-2xl rounded-full drop-shadow-2xl focus:outline-none" type="email" placeholder="Email"${ssrRenderAttr("value", email.value)} data-v-157056b0><input class="my-4 py-3 px-5 w-full text-2xl rounded-full drop-shadow-2xl focus:outline-none" type="password" placeholder="Password"${ssrRenderAttr("value", password.value)} data-v-157056b0><button type="submit" class="${ssrRenderClass([loading.value ? "opacity-50 cursor-not-allowed" : "", "my-4 bg-sky-500 px-10 py-3 text-white rounded-full drop-shadow-2xl"])}"${ssrIncludeBooleanAttr(loading.value) ? " disabled" : ""} data-v-157056b0>${ssrInterpolate(loading.value ? "Loading" : "Sign Up")}</button></form>`);
      if (errorMessage.value) {
        _push(`<div class="error-message" data-v-157056b0>${ssrInterpolate(errorMessage.value)}</div>`);
      } else {
        _push(`<!---->`);
      }
      if (successMessage.value) {
        _push(`<div class="success-message" data-v-157056b0>${ssrInterpolate(successMessage.value)} <br data-v-157056b0><a href="/login" class="underline font-bold" data-v-157056b0>Login page</a></div>`);
      } else {
        _push(`<!---->`);
      }
      _push(`</div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/register.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const register = /* @__PURE__ */ _export_sfc(_sfc_main, [["__scopeId", "data-v-157056b0"]]);

export { register as default };
//# sourceMappingURL=register-b5887bea.mjs.map
