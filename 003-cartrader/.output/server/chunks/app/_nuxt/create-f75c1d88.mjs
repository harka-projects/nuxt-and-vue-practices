import { unref, useSSRContext, ref, mergeProps } from 'vue';
import { ssrRenderAttrs, ssrInterpolate, ssrRenderComponent, ssrRenderList, ssrRenderAttr } from 'vue/server-renderer';
import { l as useState } from '../server.mjs';
import { u as useCars } from './useCars-bc9f5312.mjs';
import '../../nitro/node-server.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'node:fs';
import 'node:url';
import 'ipx';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import '@supabase/supabase-js';

const _sfc_main$4 = {
  __name: "Select",
  __ssrInlineRender: true,
  props: {
    title: String,
    name: String,
    options: Array
  },
  emits: ["changeInput"],
  setup(__props, { emit: __emit }) {
    ref("");
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "flex flex-col w-[48%] mt-2" }, _attrs))}><label class="text-cyan-500 mb-1 text-sm">${ssrInterpolate(__props.title)}</label><select class="select p-2 border w-100 rounded"><!--[-->`);
      ssrRenderList(__props.options, (option) => {
        _push(`<option${ssrRenderAttr("value", option)}>${ssrInterpolate(option)}</option>`);
      });
      _push(`<!--]--></select></div>`);
    };
  }
};
const _sfc_setup$4 = _sfc_main$4.setup;
_sfc_main$4.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/Car/Ad/Select.vue");
  return _sfc_setup$4 ? _sfc_setup$4(props, ctx) : void 0;
};
const __nuxt_component_0 = _sfc_main$4;
const _sfc_main$3 = {
  __name: "Input",
  __ssrInlineRender: true,
  props: {
    title: String,
    name: String,
    placeholder: String
  },
  emits: ["changeInput"],
  setup(__props, { emit: __emit }) {
    const state = ref("");
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "flex flex-col w-[48%] mt-2" }, _attrs))}><label class="text-cyan-500 mb-1 text-sm">${ssrInterpolate(__props.title)}</label><input type="text" class="p-2 border w-100 rounded"${ssrRenderAttr("placeholder", __props.placeholder)}${ssrRenderAttr("value", unref(state))}${ssrRenderAttr("name", __props.name)}></div>`);
    };
  }
};
const _sfc_setup$3 = _sfc_main$3.setup;
_sfc_main$3.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/Car/Ad/Input.vue");
  return _sfc_setup$3 ? _sfc_setup$3(props, ctx) : void 0;
};
const __nuxt_component_1 = _sfc_main$3;
const _sfc_main$2 = {
  __name: "TextArea",
  __ssrInlineRender: true,
  props: {
    title: String,
    name: String,
    placeholder: String
  },
  emits: ["changeInput"],
  setup(__props, { emit: __emit }) {
    const state = ref("");
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "flex flex-col w-[100%] mt-2" }, _attrs))}><label class="text-cyan-500 mb-1 text-sm">${ssrInterpolate(__props.title)}</label><textarea type="text" class="p-2 border w-100 rounded"${ssrRenderAttr("placeholder", __props.placeholder)}${ssrRenderAttr("name", __props.name)}>${ssrInterpolate(unref(state))}</textarea></div>`);
    };
  }
};
const _sfc_setup$2 = _sfc_main$2.setup;
_sfc_main$2.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/Car/Ad/TextArea.vue");
  return _sfc_setup$2 ? _sfc_setup$2(props, ctx) : void 0;
};
const __nuxt_component_2 = _sfc_main$2;
const _sfc_main$1 = {
  __name: "Image",
  __ssrInlineRender: true,
  emits: ["changeInput"],
  setup(__props, { emit: __emit }) {
    const image = useState("carImage", () => {
      return {
        preview: null,
        image: null
      };
    });
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "col-md-5 offset-md-1 mt-2 w-[100%]" }, _attrs))}><label class="text-cyan-500 mb-1 text-sm">Image *</label><form class="mt-2"><div class="form-group"><div class="relative"><input class="opacity-0 absolute cursor-pointer" type="file" accept="image/*"><span class="cursor-pointer">Browse file</span></div></div></form>`);
      if (unref(image).preview) {
        _push(`<div class="border p-2 mt-3 w-56"><img${ssrRenderAttr("src", unref(image).preview)} alt="" class="img-fluid"></div>`);
      } else {
        _push(`<!---->`);
      }
      _push(`</div>`);
    };
  }
};
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/Car/Ad/Image.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_3 = _sfc_main$1;
const _sfc_main = {
  __name: "create",
  __ssrInlineRender: true,
  setup(__props) {
    const { makes } = useCars();
    const info = useState("adInfo", () => {
      return {
        make: "",
        model: "",
        year: "",
        miles: "",
        price: "",
        city: "",
        seats: "",
        features: "",
        description: "",
        image: null
      };
    });
    const inputs = [
      { id: 1, title: "Model *", name: "model", placeholder: "Civic" },
      { id: 2, title: "Year *", name: "year", placeholder: "2019" },
      { id: 3, title: "Miles *", name: "miles", placeholder: "10000" },
      { id: 4, title: "City *", name: "city", placeholder: "Vienna, Austria" },
      { id: 5, title: "Seats *", name: "seats", placeholder: "5" },
      {
        id: 6,
        title: "Features *",
        name: "features",
        placeholder: "Leather interios, no accidents"
      }
    ];
    const onChangeInput = (data, name) => {
      info.value[name] = data;
    };
    return (_ctx, _push, _parent, _attrs) => {
      const _component_CarAdSelect = __nuxt_component_0;
      const _component_CarAdInput = __nuxt_component_1;
      const _component_CarAdTextArea = __nuxt_component_2;
      const _component_CarAdImage = __nuxt_component_3;
      _push(`<div${ssrRenderAttrs(_attrs)}><div class="mt-24"><h1 class="text-6xl">Create a New Listing</h1></div> ${ssrInterpolate(unref(info).year)} ${ssrInterpolate(unref(info).model)} <div class="shadow-rounded p-3 mt-5 flex flex-wrap justify-between">`);
      _push(ssrRenderComponent(_component_CarAdSelect, {
        title: "Make *",
        options: unref(makes),
        name: "make",
        onChangeInput
      }, null, _parent));
      _push(`<!--[-->`);
      ssrRenderList(inputs, (input) => {
        _push(ssrRenderComponent(_component_CarAdInput, {
          key: input.id,
          title: input.title,
          name: input.name,
          placeholder: input.placeholder,
          onChangeInput
        }, null, _parent));
      });
      _push(`<!--]-->`);
      _push(ssrRenderComponent(_component_CarAdTextArea, {
        title: "Description",
        name: "description",
        placeholder: "",
        onChangeInput
      }, null, _parent));
      _push(ssrRenderComponent(_component_CarAdImage, { onChangeInput }, null, _parent));
      _push(`</div></div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/profile/listings/create.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=create-f75c1d88.mjs.map
