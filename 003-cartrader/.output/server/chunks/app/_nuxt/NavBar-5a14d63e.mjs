import { _ as __nuxt_component_0$1 } from './nuxt-link-3cdb807d.mjs';
import { m as useSupabaseUser, k as useSupabaseClient } from '../server.mjs';
import { useSSRContext, mergeProps, withCtx, createTextVNode, unref, toDisplayString } from 'vue';
import { ssrRenderAttrs, ssrRenderComponent, ssrInterpolate } from 'vue/server-renderer';

const _sfc_main = {
  __name: "NavBar",
  __ssrInlineRender: true,
  setup(__props) {
    const user = useSupabaseUser();
    useSupabaseClient();
    return (_ctx, _push, _parent, _attrs) => {
      const _component_NuxtLink = __nuxt_component_0$1;
      _push(`<header${ssrRenderAttrs(mergeProps({ class: "sticky top-0 z-50 flex justify-between items-center space-x-1 border-b bg-white p-4 shadow-md" }, _attrs))}>`);
      _push(ssrRenderComponent(_component_NuxtLink, {
        class: "text-3xl font-mono",
        to: "/"
      }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`cartrader`);
          } else {
            return [
              createTextVNode("cartrader")
            ];
          }
        }),
        _: 1
      }, _parent));
      if (unref(user)) {
        _push(`<div class="flex">`);
        _push(ssrRenderComponent(_component_NuxtLink, {
          to: "/profile/listings",
          class: "mr-5"
        }, {
          default: withCtx((_, _push2, _parent2, _scopeId) => {
            if (_push2) {
              _push2(`${ssrInterpolate(unref(user).email)}`);
            } else {
              return [
                createTextVNode(toDisplayString(unref(user).email), 1)
              ];
            }
          }),
          _: 1
        }, _parent));
        _push(`<p class="cursor-pointer">Logout</p></div>`);
      } else {
        _push(`<div class="flex">`);
        _push(ssrRenderComponent(_component_NuxtLink, {
          to: "/register",
          class: "me-3"
        }, {
          default: withCtx((_, _push2, _parent2, _scopeId) => {
            if (_push2) {
              _push2(`Register`);
            } else {
              return [
                createTextVNode("Register")
              ];
            }
          }),
          _: 1
        }, _parent));
        _push(ssrRenderComponent(_component_NuxtLink, { to: "/login" }, {
          default: withCtx((_, _push2, _parent2, _scopeId) => {
            if (_push2) {
              _push2(`Login`);
            } else {
              return [
                createTextVNode("Login")
              ];
            }
          }),
          _: 1
        }, _parent));
        _push(`</div>`);
      }
      _push(`</header>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/NavBar.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};
const __nuxt_component_0 = _sfc_main;

export { __nuxt_component_0 as _ };
//# sourceMappingURL=NavBar-5a14d63e.mjs.map
