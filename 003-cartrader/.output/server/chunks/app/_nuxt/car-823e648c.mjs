import { defineComponent, ref, onErrorCaptured, withCtx, createVNode, toDisplayString, useSSRContext, computed, mergeProps, unref } from 'vue';
import { u as useCars } from './useCars-bc9f5312.mjs';
import { d as useRoute, i as __nuxt_component_2, h as useRouter } from '../server.mjs';
import { ssrRenderAttrs, ssrRenderComponent, ssrInterpolate, ssrRenderAttr, ssrRenderList } from 'vue/server-renderer';
import { u as useUtilities } from './useUtilities-986ccc79.mjs';
import { u as useHead } from './index-5a67b3af.mjs';
import '../../nitro/node-server.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'node:fs';
import 'node:url';
import 'ipx';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import '@supabase/supabase-js';

const __nuxt_component_0 = defineComponent({
  emits: {
    error(_error) {
      return true;
    }
  },
  setup(_props, { slots, emit }) {
    const error = ref(null);
    onErrorCaptured((err, target, info) => {
    });
    function clearError() {
      error.value = null;
    }
    return () => {
      var _a, _b;
      return error.value ? (_a = slots.error) == null ? void 0 : _a.call(slots, { error, clearError }) : (_b = slots.default) == null ? void 0 : _b.call(slots);
    };
  }
});
const _sfc_main$1 = {
  __name: "SideBar",
  __ssrInlineRender: true,
  setup(__props) {
    const { makes } = useCars();
    const modal = ref({
      make: false,
      location: false,
      price: false
    });
    const city = ref("");
    const priceRange = ref({
      min: "",
      max: ""
    });
    const route = useRoute();
    useRouter();
    const priceRangeText = computed(() => {
      const minPrice = route.query.minPrice;
      const maxPrice = route.query.maxPrice;
      if (!minPrice || !maxPrice)
        return "Any";
      else if (!minPrice && maxPrice)
        return `< $${maxPrice}`;
      else if (minPrice && !maxPrice)
        return `> $${minPrice}`;
      else
        return `$${minPrice} - $${maxPrice}`;
    });
    return (_ctx, _push, _parent, _attrs) => {
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "shadow border w-64 mr-10 z-30 h-[190px]" }, _attrs))}><div class="p-5 flex justify-between relative cursor-pointer border-b"><h3>Location</h3><h3 class="text-blue-400 capitalize">${ssrInterpolate(unref(route).params.city)}</h3>`);
      if (unref(modal).location) {
        _push(`<div class="absolute border shadow left-56 p-5 top-1 -m-1 bg-white"><input type="text" class="border p-1 rounded"${ssrRenderAttr("value", unref(city))}><button class="bg-blue-400 w-full mt-2 rounded text-white p-1"> Apply </button></div>`);
      } else {
        _push(`<!---->`);
      }
      _push(`</div><div class="p-5 flex justify-between relative cursor-pointer border-b"><h3>Make</h3><h3 class="text-blue-400 capitalize">${ssrInterpolate(unref(route).params.make || "All")}</h3>`);
      if (unref(modal).make) {
        _push(`<div class="absolute border shadow left-56 p-5 top-1 -m-1 w-[600px] flex justify-between flex-wrap bg-white"><!--[-->`);
        ssrRenderList(unref(makes), (make) => {
          _push(`<h4 class="w-1/3">${ssrInterpolate(make)}</h4>`);
        });
        _push(`<!--]--></div>`);
      } else {
        _push(`<!---->`);
      }
      _push(`</div><div class="p-5 flex justify-between relative cursor-pointer border-b"><h3>Price</h3><h3 class="text-blue-400 capitalize">${ssrInterpolate(unref(priceRangeText))}</h3>`);
      if (unref(modal).price) {
        _push(`<div class="absolute border shadow left-56 p-5 top-1 -m-1 bg-white"><input type="number" placeholder="Min"${ssrRenderAttr("value", unref(priceRange).min)} class="border p-1 rounded"><input type="number" placeholder="Max"${ssrRenderAttr("value", unref(priceRange).max)} class="border p-1 rounded"><button class="bg-blue-400 w-full mt-2 rounded text-white p-1"> Apply </button></div>`);
      } else {
        _push(`<!---->`);
      }
      _push(`</div></div>`);
    };
  }
};
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/Car/SideBar.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_1 = _sfc_main$1;
const _sfc_main = {
  __name: "car",
  __ssrInlineRender: true,
  setup(__props) {
    const route = useRoute();
    const { toTitleCase } = useUtilities();
    useHead({
      title: `${route.params.make ? `${toTitleCase(route.params.make)}s` : "Cars"} in ${toTitleCase(route.params.city)}`
    });
    return (_ctx, _push, _parent, _attrs) => {
      const _component_NuxtErrorBoundary = __nuxt_component_0;
      const _component_CarSideBar = __nuxt_component_1;
      const _component_NuxtPage = __nuxt_component_2;
      _push(`<div${ssrRenderAttrs(_attrs)}><div class="mt-32 flex">`);
      _push(ssrRenderComponent(_component_NuxtErrorBoundary, null, {
        error: withCtx(({ error }, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`<div class="text-center mx-auto flex flex-col"${_scopeId}><h1 class="text-5xl text-red-600 mb-4"${_scopeId}>Something went wrong.</h1><code${_scopeId}>${ssrInterpolate(error)}</code><button class="text-white bg-blue-400 px-10 py-3 rounded mt-4"${_scopeId}> Go back </button></div>`);
          } else {
            return [
              createVNode("div", { class: "text-center mx-auto flex flex-col" }, [
                createVNode("h1", { class: "text-5xl text-red-600 mb-4" }, "Something went wrong."),
                createVNode("code", null, toDisplayString(error), 1),
                createVNode("button", {
                  class: "text-white bg-blue-400 px-10 py-3 rounded mt-4",
                  onClick: ($event) => error.value = null
                }, " Go back ", 8, ["onClick"])
              ])
            ];
          }
        }),
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(ssrRenderComponent(_component_CarSideBar, null, null, _parent2, _scopeId));
            _push2(ssrRenderComponent(_component_NuxtPage, null, null, _parent2, _scopeId));
          } else {
            return [
              createVNode(_component_CarSideBar),
              createVNode(_component_NuxtPage)
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</div></div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/city/[city]/car.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=car-823e648c.mjs.map
