import { _ as __nuxt_component_0 } from './nuxt-link-3cdb807d.mjs';
import { withCtx, createTextVNode, unref, useSSRContext, mergeProps } from 'vue';
import { ssrRenderAttrs, ssrRenderComponent, ssrRenderList, ssrRenderAttr, ssrInterpolate } from 'vue/server-renderer';
import { u as useCars } from './useCars-bc9f5312.mjs';
import '../../nitro/node-server.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'node:fs';
import 'node:url';
import 'ipx';
import '../server.mjs';
import 'unhead';
import '@unhead/shared';
import 'vue-router';
import '@supabase/supabase-js';

const _sfc_main$1 = {
  __name: "ListingCard",
  __ssrInlineRender: true,
  props: {
    listing: Object
  },
  setup(__props) {
    return (_ctx, _push, _parent, _attrs) => {
      const _component_NuxtLink = __nuxt_component_0;
      _push(`<div${ssrRenderAttrs(mergeProps({ class: "shadow rounded overflow-hidden flex justify-between mb-4" }, _attrs))}><div class="flex"><img${ssrRenderAttr("src", __props.listing.url)} alt="" class="w-80 mr-3 h-44"><div class="p-3"><h1 class="text-2xl">${ssrInterpolate(__props.listing.name)}</h1><p class="text-blue-400">$${ssrInterpolate(__props.listing.price)}</p></div></div><div class="p-3 flex">`);
      _push(ssrRenderComponent(_component_NuxtLink, {
        to: `/profile/listings/view/${__props.listing.id}`,
        class: "text-blue-400 mr-4"
      }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`View`);
          } else {
            return [
              createTextVNode("View")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`<p class="text-red-400 cursor-pointer">Delete</p></div></div>`);
    };
  }
};
const _sfc_setup$1 = _sfc_main$1.setup;
_sfc_main$1.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("components/Car/ListingCard.vue");
  return _sfc_setup$1 ? _sfc_setup$1(props, ctx) : void 0;
};
const __nuxt_component_1 = _sfc_main$1;
const _sfc_main = {
  __name: "index",
  __ssrInlineRender: true,
  setup(__props) {
    const { listings } = useCars();
    return (_ctx, _push, _parent, _attrs) => {
      const _component_NuxtLink = __nuxt_component_0;
      const _component_CarListingCard = __nuxt_component_1;
      _push(`<div${ssrRenderAttrs(_attrs)}><div class="flex justify-between mt-24 items-center"><h1 class="text-6\xD7l">My Listings</h1>`);
      _push(ssrRenderComponent(_component_NuxtLink, {
        to: "/profile/listings/create",
        class: "w-9 h-9 bg-blue-400 flex justify-center items-center rounded-full text-white font-bold cursor-pointer"
      }, {
        default: withCtx((_, _push2, _parent2, _scopeId) => {
          if (_push2) {
            _push2(`+`);
          } else {
            return [
              createTextVNode("+")
            ];
          }
        }),
        _: 1
      }, _parent));
      _push(`</div><div class="shadow rounded p-3 mt-5"><!--[-->`);
      ssrRenderList(unref(listings), (listing) => {
        _push(ssrRenderComponent(_component_CarListingCard, {
          key: listing.id,
          listing
        }, null, _parent));
      });
      _push(`<!--]--></div></div>`);
    };
  }
};
const _sfc_setup = _sfc_main.setup;
_sfc_main.setup = (props, ctx) => {
  const ssrContext = useSSRContext();
  (ssrContext.modules || (ssrContext.modules = /* @__PURE__ */ new Set())).add("pages/profile/listings/index.vue");
  return _sfc_setup ? _sfc_setup(props, ctx) : void 0;
};

export { _sfc_main as default };
//# sourceMappingURL=index-2e2c2442.mjs.map
