const client_manifest = {
  "_NavBar.ec074c5b.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "NavBar.ec074c5b.js",
    "imports": [
      "_nuxt-link.ab2c88cb.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_fetch.8747e2c6.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "fetch.8747e2c6.js",
    "imports": [
      "_vue.f36acd1f.ea63795a.js",
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_nuxt-link.ab2c88cb.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "nuxt-link.ab2c88cb.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "_useCars.364fa0e5.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "useCars.364fa0e5.js"
  },
  "_useUtilities.17dd72cc.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "useUtilities.17dd72cc.js"
  },
  "_vue.f36acd1f.ea63795a.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "vue.f36acd1f.ea63795a.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ]
  },
  "assets/heartFilled.png": {
    "resourceType": "image",
    "prefetch": true,
    "mimeType": "image/png",
    "file": "heartFilled.7a401f85.png",
    "src": "assets/heartFilled.png"
  },
  "assets/heartOutline.png": {
    "resourceType": "image",
    "prefetch": true,
    "mimeType": "image/png",
    "file": "heartOutline.cdbc192a.png",
    "src": "assets/heartOutline.png"
  },
  "layouts/custom.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "custom.f39e3f6c.js",
    "imports": [
      "_NavBar.ec074c5b.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_nuxt-link.ab2c88cb.js"
    ],
    "isDynamicEntry": true,
    "src": "layouts/custom.vue"
  },
  "layouts/default.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "default.9cf1bf3d.js",
    "imports": [
      "_NavBar.ec074c5b.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_nuxt-link.ab2c88cb.js"
    ],
    "isDynamicEntry": true,
    "src": "layouts/default.vue"
  },
  "node_modules/nuxt/dist/app/entry.js": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "dynamicImports": [
      "layouts/custom.vue",
      "layouts/default.vue"
    ],
    "file": "entry.5e3184e2.js",
    "isEntry": true,
    "src": "node_modules/nuxt/dist/app/entry.js",
    "_globalCSS": true
  },
  "pages/car/[name]-[id].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "_name_-_id_.ff5cbf00.js",
    "imports": [
      "_fetch.8747e2c6.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_useUtilities.17dd72cc.js",
      "_vue.f36acd1f.ea63795a.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/car/[name]-[id].vue"
  },
  "pages/city/[city]/car.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "car.5bbb1ac4.js",
    "imports": [
      "_useCars.364fa0e5.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_useUtilities.17dd72cc.js",
      "_vue.f36acd1f.ea63795a.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/city/[city]/car.vue"
  },
  "pages/city/[city]/car/[[make]].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "assets": [
      "heartFilled.7a401f85.png",
      "heartOutline.cdbc192a.png"
    ],
    "file": "_make_.c9c74b6c.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_fetch.8747e2c6.js",
      "_vue.f36acd1f.ea63795a.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/city/[city]/car/[[make]].vue"
  },
  "heartFilled.7a401f85.png": {
    "file": "heartFilled.7a401f85.png",
    "resourceType": "image",
    "prefetch": true,
    "mimeType": "image/png"
  },
  "heartOutline.cdbc192a.png": {
    "file": "heartOutline.cdbc192a.png",
    "resourceType": "image",
    "prefetch": true,
    "mimeType": "image/png"
  },
  "pages/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.3e544a46.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_vue.f36acd1f.ea63795a.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/index.vue"
  },
  "pages/login.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "login.8c512460.css",
    "src": "pages/login.css"
  },
  "pages/login.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "login.bb1e7ffc.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/login.vue"
  },
  "login.8c512460.css": {
    "file": "login.8c512460.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  },
  "pages/profile/listings/create.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "create.56f4fa2e.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js",
      "_useCars.364fa0e5.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/profile/listings/create.vue"
  },
  "pages/profile/listings/index.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "index.585cbf08.js",
    "imports": [
      "_nuxt-link.ab2c88cb.js",
      "node_modules/nuxt/dist/app/entry.js",
      "_useCars.364fa0e5.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/profile/listings/index.vue"
  },
  "pages/profile/listings/view/[id].vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "file": "_id_.b2fe2021.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/profile/listings/view/[id].vue"
  },
  "pages/register.css": {
    "resourceType": "style",
    "prefetch": true,
    "preload": true,
    "file": "register.1a7f7bec.css",
    "src": "pages/register.css"
  },
  "pages/register.vue": {
    "resourceType": "script",
    "module": true,
    "prefetch": true,
    "preload": true,
    "css": [],
    "file": "register.c9ebe3b1.js",
    "imports": [
      "node_modules/nuxt/dist/app/entry.js"
    ],
    "isDynamicEntry": true,
    "src": "pages/register.vue"
  },
  "register.1a7f7bec.css": {
    "file": "register.1a7f7bec.css",
    "resourceType": "style",
    "prefetch": true,
    "preload": true
  }
};

export { client_manifest as default };
//# sourceMappingURL=client.manifest.mjs.map
