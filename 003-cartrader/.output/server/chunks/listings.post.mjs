import { d as defineEventHandler, r as readBody, c as createError } from './nitro/node-server.mjs';
import Joi from 'joi';
import { PrismaClient } from '@prisma/client';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'node:fs';
import 'node:url';
import 'ipx';

const prisma = new PrismaClient();
const schema = Joi.object({
  make: Joi.string().required(),
  model: Joi.string().required(),
  year: Joi.number().min(1886).max((/* @__PURE__ */ new Date()).getFullYear() + 1),
  miles: Joi.number().required().min(0),
  city: Joi.string().required().min(2),
  numberOfSeats: Joi.number().required().min(1).max(1e3),
  description: Joi.string().required().min(20),
  features: Joi.array().required().items(Joi.string()),
  image: Joi.string().required(),
  listerId: Joi.string().required(),
  price: Joi.number().required().min(0),
  name: Joi.string().required()
});
const listings_post = defineEventHandler(async (e) => {
  const body = await readBody(e);
  const { error, value } = await schema.validate(body);
  if (error) {
    throw createError({
      statusCode: 412,
      statusMessage: error.message
    });
  }
  const {
    image,
    name,
    numberOfSeats,
    features,
    description,
    miles,
    price,
    listerId,
    city,
    make,
    model
  } = body;
  const car = await prisma.car.create({
    data: {
      image,
      name,
      numberOfSeats,
      features,
      description,
      miles,
      price,
      listerId,
      city: city.toLowerCase(),
      make,
      model
    }
  });
  return car;
});

export { listings_post as default };
//# sourceMappingURL=listings.post.mjs.map
