import { d as defineEventHandler, g as getQuery } from './nitro/node-server.mjs';
import { c as cars } from './cars.mjs';
import 'node:http';
import 'node:https';
import 'fs';
import 'path';
import 'node:fs';
import 'node:url';
import 'ipx';

const _city__get = defineEventHandler((e) => {
  const { city } = e.context.params;
  const { make, minPrice, maxPrice } = getQuery(e);
  let filteredCars = cars;
  filteredCars = filteredCars.filter((car) => {
    var _a;
    return ((_a = car.city) == null ? void 0 : _a.toLowerCase()) === city.toLowerCase();
  });
  if (make) {
    filteredCars = filteredCars.filter((car) => {
      var _a;
      return ((_a = car.make) == null ? void 0 : _a.toLowerCase()) === make.toLowerCase();
    });
  }
  if (minPrice) {
    filteredCars = filteredCars.filter((car) => {
      return car.price >= parseInt(minPrice);
    });
  }
  if (maxPrice) {
    filteredCars = filteredCars.filter((car) => {
      return car.price <= parseInt(maxPrice);
    });
  }
  return filteredCars;
});

export { _city__get as default };
//# sourceMappingURL=_city_.get.mjs.map
